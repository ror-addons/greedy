Greedy = {}

function Greedy.Initialize()
	Greedy.Save = Greedy.Save or {}
	
	Greedy.Save.disabled = Greedy.Save.disabled or false
	
	RegisterEventHandler(SystemData.Events.GROUP_UPDATED, "Greedy.OnGroupUpdated")
	
	if(LibSlash) then
		LibSlash.RegisterSlashCmd("greedy", function(msg) Greedy.Slash(msg) end)
	end
end

function Greedy.Shutdown()
	UnregisterEventHandler(SystemData.Events.GROUP_UPDATED, "Greedy.OnGroupUpdated")
end

function Greedy.OnGroupUpdated()
	if Greedy.Save.disabled then return end
	local current = GameData.Player.Group.Settings.noNeedOnGreed
	
	if(GameData.Player.isGroupLeader and not current) then
		GameData.Player.Group.Settings.noNeedOnGreed = true
		SetGroupNeedMode(GameData.Player.Group.Settings.noNeedOnGreed)
	end
end

function Greedy.Slash(msg)
	local txt = string.lower(msg)
	
	if(txt == "enable") then
		Greedy.Save.disabled = false
		Greedy.Print("auto Need on Use enabled.")
	elseif(txt == "disable") then
		Greedy.Save.disabled = true
		Greedy.Print("auto Need on Use disabled.")
	else
		Greedy.Print("enable - enables auto Need on Use.")
		Greedy.Print("disable - disables auto Need on Use.")
	end
end

function Greedy.Print(txt)
	EA_ChatWindow.Print(towstring(txt))
end